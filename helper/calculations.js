module.exports = function makeJson(storeRes, temp) {
    if (temp == 'viewCart') return storeRes.viewCarts.body[0];
    else if (temp == 'configuration') return configData(storeRes, "SBO Monthly")
}

function spToUndscre(s){
    return s.replace(/ /g, '_')
}

function UndscreToSp(s){
    return s.replace(/_/g, ' ')
}

function configData(storeRes, selComo) {
    // let comodities = storeRes.store.comodities;
    // let curComo = comodities.filter(each => { return each.comodity == selComo; });
    // let dataset = curComo[0];
    // let addonArr =  comodities.filter(each => { return (each.discount_policy == dataset.discount_policy && each.nature=='addon'); });
    // dataset.addons = {}
    // for(let i=0;i<addonArr.length;i++)
    //     dataset.addons[spToUndscre(addonArr[i].comodity)] = addonArr[i];
    // console.log()
    let dataset = {
        "comodity":"SBO Monthly",
        "store":"Global(USD)",
        "nature":"customizable",
        "sales_type":"subscription",
        "image":"SBO",
        "initial_discount":0,
        "grace_period":0,
        "setup_charge":0,
        "base_price":8,
        "discount_policy":"SBO Monthly Discount",
        "status":"available",
        "product":null,
        "min_qty":1,
        "cat":"SBO Switch",
        "currency":"USD",
        "payment_options":[
            "COD"
        ],
        "category":"SBO Switch",
        "items":[ ],
        "b_cycle_arr": [{bc:"Daily", factor: 0},{bc: "Monthly", factor: 10}, {bc: "Yearly", factor: 15}],
        "b_cycle": "Daily",
        "base_b_cycle": "Daily",
        "gtotal": 0,
        "selected_addons":["Dedicatd_IP"],
        "image": "./images/pp.jpg",
        "addons": {
            "Dedicatd_IP": {
                "comodity":"Dedicatd IP",
                "store":"Global(USD)",
                "nature":"addon",
                "sales_type":"subscription",
                "image":"SBO",
                "initial_discount":0,
                "grace_period":0,
                "setup_charge":0,
                "base_price":0,
                "discount_policy":"SBO Monthly Discount",
                "status":"available",
                "product":"Dedicated IP",
                "min_qty":0,
                "cat":"SBO Switch",
                "currency":"USD",
                "payment_options":[
                    "COD"
                ],
                "category":"SBO Switch",
                "quantity": 0,
                "q_factor": 0,
                "qf_range": {"0+": 0},
                "total": 0
            },
            "NonProxy_Channel":{
                "comodity":"NonProxy Channel",
                "store":"Global(USD)",
                "nature":"addon",
                "sales_type":"subscription",
                "image":"SBO",
                "initial_discount":2,
                "grace_period":0,
                "setup_charge":5,
                "base_price":20,
                "discount_policy":"SBO Monthly Discount",
                "status":"available",
                "product":"NonProxy Channel",
                "min_qty":30,
                "cat":"SBO Switch",
                "currency":"USD",
                "payment_options":[
                    "COD"
                ],
                "category":"SBO Switch",
                "quantity": 30,
                "q_factor": 0,
                "qf_range": {"30-40": 0, "41-50": 5, "51-70": 10, "70+": 15},
                "total": 690,
            },
            "SBO_Channel":{
                "comodity":"SBO Channel",
                "store":"Global(USD)",
                "nature":"addon",
                "sales_type":"subscription",
                "image":"SBO",
                "initial_discount":2,
                "grace_period":0,
                "setup_charge":3,
                "base_price":12,
                "discount_policy":"SBO Monthly Discount",
                "status":"available",
                "product":"SBO Channel",
                "min_qty":4,
                "cat":"SBO Switch",
                "currency":"USD",
                "payment_options":[
                    "COD"
                ],
                "category":"SBO Switch",
                "category":"SBO Switch",
                "quantity": 4,
                "q_factor": 0,
                "qf_range": {"4-10": 0, "11-20": 5, "21-40": 10, "40+": 15},
                "total": 52,
            }
        }
    }
    
    return dataset;
}