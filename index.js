const makeJson  = require('./helper/calculations.js');
const express = require('express');
var bodyParser = require('body-parser');
var storeRes = require('./routes/store.json');

const app = express();
app.use(express.static('public'));
app.set('view engine', 'pug');
const port = 3333;

// var jsonParser = bodyParser.json()
var urlencodedParser = bodyParser.urlencoded({ extended: false });

function getTotal(a, b){
    return '$'+(a+b);
}

function getAge(years){
	return 'You are '+ years + ' years old.';
}

function getDataset(){
    return {loc:'dhaka', country: "bd" };
}

function spToUndscre(s){
    return s.replace(/ /g, '_')
}

function UndscreToSp(s){
    return s.replace(/_/g, ' ')
}



app.get('/', function (req, res) {
    res.render('index', { title: 'Hey', message: 'Hello there!' });
})

app.post('*', urlencodedParser, function (req, res) {
    let dataR = makeJson(storeRes, req.body.template);
    response = {  
        // first_name:req.body.first_name,  
        // last_name:req.body.last_name,
        template:req.body.template,
        // getTotal: getTotal,
        // getAge: getAge,
        // getDataset: getDataset,
        spToUndscre:spToUndscre,
        UndscreToSp:UndscreToSp,
        data: dataR,
	};  
    // console.log(response);  
    res.render(response.template, response);
    res.end();
})

app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}`);
})

// var express = require('express');
// var app = express();

// app.get('/', function(req, res){
//    res.send("Hello world!");
// });

// app.listen(3333);